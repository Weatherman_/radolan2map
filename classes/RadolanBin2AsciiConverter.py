'''
RadolanBin2AsciiConverter

original binary RADOLAN file to ESRI ASCII GRID (to convert this to GeoTIFF afterwards)

Created on 29.07.2019
@author: Weatherman
'''

#import os
from os import path
import sys
import numpy as np

from NumpyRadolanReader import NumpyRadolanReader

NODATA = -1.0


class RadolanBin2AsciiConverter:
    '''
    classdocs
    '''
    
    
    def __init__(self, radolan_file, out_dir, exclude_zeroes=False):
        '''
        Constructor
        
        exclude_zeroes: exclude zeroes by setting them to NaN and making them invisible on map.
        '''
        
        self.out("<- '{}', '{}'".format(radolan_file, out_dir))
        
        #
        # Übergeben
        #
        
        self._out_dir = out_dir
        
        
        # init. reader:
        self._nrr = NumpyRadolanReader(radolan_file)    # FileNotFoundError
        self._nrr.exclude_zeroes = exclude_zeroes
        
        
        #
        # Ermittelt
        #
        
        self._full_asc_filename = None
    
    
    
    def __str__(self):
        return self.__class__.__name__
    
    
    def out(self, s, ok=True):
        if ok:
            print("{}: {}".format(self, s))
        else:
            print("{}: {}".format(self, s), file=sys.stderr)
    
    
    
    
    def run(self):
        """
        Kapselt die verschiedenen internen Methoden (ganze RADOLAN-
        Datenverarbeitung als Schnittstelle nach außen.
        """
        
        self.out("run()")
        
        
        """
        Read
        """
        
        self._nrr.read()    # Exception
        
        
        
        nrows, ncols = self._nrr.data.shape
        
        
        """
        Write
        """
        
        if nrows == 1100:    # erweitertes nationales Komposit, RADKLIM
            llcorner = -443462, -4758645
        elif nrows == 900:    # nationales Komposit
            llcorner = -523462, -4658645
        elif nrows == 1500:    # mitteleuropäisches Komposit
            llcorner = -673462, -5008645
        else:
            raise NotImplementedError
        
        
        gis_header_template = '''\
ncols     {}
nrows     {}
xllcorner {}
yllcorner {}
cellsize  1000
nodata_value {}'''
        
        # The * before 'tup' tells Python to unpack the tuple into separate arguments,
        # as if you called s.format(tup[0], tup[1], tup[2]) instead.
        #header = gis_header_template.format(*dim, *llcorner, NODATA)
        # -> geht nur Python3
        gis_header = gis_header_template.format(ncols, nrows, llcorner[0], llcorner[1], NODATA)
        
        
        #out_file_bn = path.basename(radolan_file)
        out_file_bn = self._nrr.simple_name + ".asc"
        
        full_asc_filename = path.join(self._out_dir, out_file_bn)    # produced in temp. dir
        self._write_ascii_grid(gis_header, full_asc_filename)
        """
        if np.amax(data_clutter) > 0.0:
            save_file = path.join(self._temp_dir, out_file_bn + "_clutter.asc")
            self._write_ascii_grid(data_clutter, gis_header, save_file)
        
        if np.amax(data_ground) > 0.0:
            save_file = path.join(self._temp_dir, out_file_bn + "_ground.asc")
            self._write_ascii_grid(data_ground, gis_header, save_file)
        
        if np.amax(data_nodata) > 0.0:
            save_file = path.join(self._temp_dir, out_file_bn + "_nodata.asc")
            self._write_ascii_grid(data_nodata, gis_header, save_file)
        """
        # Set:
        self._full_asc_filename = full_asc_filename
    
    
    
    
    def _write_ascii_grid(self, gis_header, full_file_name):
        #if not full_file_name:
        #    raise OSError("_write_ascii_grid(): 'full_file_name' not specified!")
        
        # precision: 1.0, 0.1, 0.01
        if self._nrr.precision == 0.1:
            fmt = '%.1f'
        elif self._nrr.precision == 0.01:
            fmt = '%.2f'
        else:
            fmt = '%d'    # as integer
        
        np_data = np.copy(self._nrr.data)    # otherwise values will be changed
        
        #mask = np.isnan(np_data)
        #np_data[mask] = NODATA
        # -> ok
        np_data[np.isnan(np_data)] = NODATA    # better?
        
        # np.flipud(): Flip array in the up/down direction.
        np.savetxt(full_file_name, np.flipud(np_data), fmt=fmt,
                   delimiter=' ', newline='\n', header=gis_header, comments='') # footer=''
        
        self.out("_write_ascii_grid(): -> {}".format(full_file_name))
    
    
    
    """
    Properties
    """
    
    @property
    def rvp_to_mm(self):
        return self._nrr.rx_in_mm
    @rvp_to_mm.setter
    def rvp_to_mm(self, b):
        self._nrr.rx_in_mm = b
    
    @property
    def prod_id(self):
        return self._nrr.prod_id
    
    @property
    def is_radklim(self):
        return self._nrr.is_radklim
    
    @property
    def precision(self):
        return self._nrr.precision
    
    @property
    def date(self):
        return self._nrr.datetime
    
    @property
    def layer_name(self):
        return self._nrr.simple_name
    
    @property
    def full_asc_filename(self):
        return self._full_asc_filename
    
    @property
    def statistics(self):
        return self._nrr.get_statistics()
    
    

def test_product_get_id(radolan_file):
    ''' Special function, called separately
    True if product is X-product (RX, WX, EX, that means coded in RVP6-units);
    False if not '''
    
    print('test_product_get_id("{}")'.format(radolan_file))
    
    nrr = NumpyRadolanReader(radolan_file)    # FileNotFoundError
    # -> file handle open
    header = nrr._read_radolan_header()
    nrr._fobj.close()
    
    prod_id = header[:2]
    
    print("  {}".format(header))
    print("  product id:", prod_id)
    
    return prod_id
    
    
