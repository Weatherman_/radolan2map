# -*- coding: utf-8 -*-

'''
Model

Manages everything with paths (from config), QGIS environment,
working on file system (create, delete).
For the special works, such as transformations, own classes exists.

Created on 23.11.2017
last change: 08.11.2019
@author: Weatherman
'''

import os
from os import path
from pathlib import Path
import sys

from configparser import ConfigParser, NoOptionError
from glob import glob
#import platform    # to determine wether Windows or not

from qgis.core import QgsApplication, QgsProject, QgsVectorLayer
from qgis.core import QgsCoordinateReferenceSystem, QgsLayerDefinition, QgsLayerTreeGroup
# For the QGIS printing template:
from qgis.PyQt.QtXml import QDomDocument

from Regnie2CSV import Regnie2CSV
import def_products       # File 'def_products.py'
import def_projections    # File 'def_projections.py'
CONFIG_NAME = "config.ini"



class Model:
    '''
    classdocs
    '''
    
    
    def __init__(self):
        '''
        Constructor
        '''
        
        print(self)
        
        #self._plugin_dir = path.abspath(path.join(path.dirname(__file__), os.pardir))
        #self._plugin_dir = Path(self._plugin_dir)
        self._plugin_dir = Path(__file__).resolve().parent.parent    # classes/Model.py -> classes -> .. 
        
        config_file = self._plugin_dir / CONFIG_NAME
        
        if not path.exists(config_file):
            raise FileNotFoundError("Config file '{}'".format(config_file))
        
        #
        # Parameter
        #
        self._config_file = config_file
        
        #
        # Ermittelt
        #
        
        # Objekt, auf das wir im Folgenden immer zugreifen.
        # Statisch ging es leider aus welchen Gründen auch immer (war in anderen Methoden 'None').
        self._config = ConfigParser()
        self._config.read(config_file)
        
        self._product_defs = def_products.dict_titles
        self.out("{}: {} product titles loaded.".format(def_products.__name__, len(self._product_defs)))
        
        self._data_root  = None    # Path
        self._data_dir   = None    # Path    <root>/radolan or .../radklim or .../regnie
        
        self._list_of_used_files = []
        
        
        
    
    def __str__(self):
        return self.__class__.__name__
    
    
    def out(self, s, ok=True):
        if ok:
            print("{}: {}".format(self, s))
        else:
            print("{}: {}".format(self, s), file=sys.stderr)
    
    
    def set_data_dir(self, subdir_name):
        self._data_dir = self.data_root / subdir_name
        self.out("set_data_dir(): {}".format(self._data_dir))
    
    
    def create_storage_folder_structure(self, temp_dir=None):
        '''
        affects filesystem
        
        temp_dir: if it is set, then only create the temp_dir
        '''
        
        self.out("create_storage_folder_structure()")
        
        
        if temp_dir:
            """
            temp dir    (with cleaning)
            """
            
            temp_dir = self.temp_dir
            
            try:
                os.makedirs(temp_dir)    # inclusive data dir
            except FileExistsError:
                # Remove old content:
                print("  remove temp dir contents...")
                # If ignore_errors is set, errors are ignored; otherwise, if onerror is set, it is called to handle ...
                #shutil.rmtree(temp_dir)
                #shutil.rmtree(path, ignore_errors=True)
                # -> Deleting a whole directory is problematic ("in use").
                
                tempdir_pattern = path.join(str(temp_dir), '*')
                
                for f in glob(tempdir_pattern):
                    try:
                        os.remove(f)
                        """ Curiously an exception occurred in Windows (7) when loading data
                    multiple times. Somehow the system does not let go of the data it touches. """
                    except WindowsError as e:    # only available on Windows
                        self.out("ERROR: {}\n  try to ignore.".format(e))
                # for
            else:
                self.out("temp dir created: {}".format(temp_dir))
            
            return
        # if temp_dir
        
        
        """
        temp dir    maybe root was changed, so check again...
        data dir
        """
        
        for _dir in (self.temp_dir, self._data_dir):
            
            try:
                os.makedirs(_dir)
                self.out("makedirs(): {}".format(_dir))
            except FileExistsError:
                pass
        
        
        
    
    
    def write_history_file(self):
        """
        hint: 'l_files' was filled in method "_init_dock()"
        """
        
        if not self.list_of_used_files:
            return
        
        
        _max = 9
        
        # write the _max items back:
        with open(self.history_file, 'w') as hf:
            for i, line in enumerate(self.list_of_used_files):
                if i >= _max:
                    break
                hf.write(line + '\n')
        
        self.out("selected file stored in '{}'".format(self.history_file))
        
    
    
    def create_qdocument_from_print_template_content(self):
        print_template = self.default_print_template
        
        self.out("create_qdocument_from_print_template_content(): {}".format(print_template))
        
        if not print_template:
            raise FileNotFoundError()
        
        # Load template
        with open(print_template) as templateFile:
            print_template_content = templateFile.read()
        
        q_xmldoc = QDomDocument()
        # If namespaceProcessing is true, the parser recognizes namespaces in the XML file and sets
        # the prefix name, local name and namespace URI to appropriate values. If namespaceProcessing
        # is false, the parser does no namespace processing when it reads the XML file.
        q_xmldoc.setContent(print_template_content, False)    # , bool namespaceProcessing
        
        return q_xmldoc
        
    
    def create_default_project(self):
        ''' Load a prepared template project.
        see https://docs.qgis.org/testing/en/docs/pyqgis_developer_cookbook/loadproject.html '''
        
        self.out("create_default_project()")
        
        
        # Get the project instance
        project = QgsProject.instance()    # current project
        
        # Load another project:
        project_file = self.default_project_file
        self.out("loading template project: {}".format(project_file))
        project.read(project_file)
        
        
        """ Related to Windows / QGIS 3.10
        Phenomenon 1: after loading the template project,
        QGIS map canvas 'jumps outside' the layer extents, even though the template project
        was saved with map centered to Germany.
        Phenomenon 2: after loading the template project (actually) with crs EPSG:3035
        but the mouse pointer on the map showed WGS1984 coordinates.
        So the assumption was to try to fix that by extra setting up the project crs "from outside"
        to EPSG:3035.
        But the problem wasn't fixed with that.
        The crs setting below was actually inserted for Windows but we leave it here for all platforms.
        It can not hurt to set up the crs from outside again for the new project.
        """
        crs = QgsCoordinateReferenceSystem(3035)
        project.setCrs(crs)
        
        return project
    
    
    
    def load_radarnetwork_layer(self):
        ''' load DWDs radar network consisting of two layers (points and buffer)
        by QgsLayerDefinition file '''
        
        
        layergroup_name = "RadarNetwork"
        
        
        root = QgsProject.instance().layerTreeRoot()
        
        """
        Check/uncheck radar network group layer if existing
        """
        
        # find group layers:
        '''
        for child in root.children():
            if isinstance(child, QgsLayerTreeGroup) and child.name() == layergroup_name:
                self.out("LayerGroup '{}' already exists.".format(layergroup_name), False)
                return
        ''' # is OK, it works
        # simpler method:
        group = root.findGroup(layergroup_name)
        if group:
            #self.out("LayerGroup '{}' already exists.".format(layergroup_name), False)
            state = group.isVisible()
            group.setItemVisibilityChecked(not state)
            return
        
        
        """
        Create radar network group layer
        """
        
        layer_def_file = self.plugin_dir / "example/shapes/RadarNetwork/dwd_radarnetwork.qlr"
        
        if not layer_def_file.exists():
            self.out("layer definition file '{}' doesn't exist.".format(layer_def_file), False)
            return
        
        """ OK for buffer layer only:
        shp = self._model.plugin_dir / "example/shapes/RadarNetwork/radarbuffer_150km.gpkg"
        vlayer_radar = QgsVectorLayer(str(shp), 'radar_buffer', 'ogr')
        #print("isValid:", vlayer_radar.isValid())
        #crs_radolan = QgsCoordinateReferenceSystem()
        #crs_radolan.createFromProj4(self._model.projection_radolan)
        #vlayer_radar.setCrs(crs_radolan)
        QgsProject.instance().addMapLayer(vlayer_radar)
        """
        
        #group = root.addGroup("RadarNetwork")    # ok, appended
        group = root.insertGroup(0, layergroup_name)    # first position
        self.out("loading layer definition file '{}'".format(layer_def_file))
        QgsLayerDefinition().loadLayerDefinition(str(layer_def_file), QgsProject.instance(), group)
        
    
    
    def convert_regnie_raster2csv_create_regnie_vector_layer(self, regnie_raster_file, out_dir):
        '''
        - Usage of the REGNIE-Class
        - Pass exceptions to caller
        '''
        
        self.out("convert_regnie_raster2csv_create_regnie_vector_layer('{}', '{}')"
                 .format(regnie_raster_file, out_dir))
        
        
        regnie_converter = Regnie2CSV(regnie_raster_file)
        """
        There are quite a few missing values in the Regnie grid; it therefore makes sense
        not to write these values out in favor of smaller output files.
        -> about 7.5 MB compared to 13 MB
        """
        regnie_converter.ignore_missings = True
        regnie_converter.convert(out_dir=out_dir)
        
        regnie_csv_file = regnie_converter.csv_pathname    # Path
        
        '''
        LAT,LON,ID,VAL
        55.058333,8.383333,1,7
        55.058333,8.400000,2,7
        '''
        #uri = "file:///{}?encoding=UTF-8&delimiter=,&xField=LON&yField=LAT&crs=EPSG:4326".format(regnie_csv_file)
        # -> was working, simpler variant:
        #uri = "{}{}?delimiter=,&xField=LON&yField=LAT".format('file:///', regnie_csv_file)
        # but not on Windows - needs crs specified:
        uri = "{}{}?delimiter=,&xField=LON&yField=LAT&crs=EPSG:4326".format('file:///', regnie_csv_file)
        
        self.out(" uri: {}".format(uri))
        
        # Make a vector layer:
        csv_layer = QgsVectorLayer(uri, regnie_csv_file.name, 'delimitedtext')
        
        if not csv_layer.isValid():
            raise OSError("File '{}' couldn't loaded!")
            
        
        # Add CSV data:
        
        cpi = regnie_converter.char_period_indicator
        self.out(" char period indicator = '{}'".format(cpi))
        
        d_regnie_qml = {
            'd': 'regnie_daily.qml',
            'm': 'regnie_monthly.qml',
            'y': 'regnie_yearly.qml',
            }
        
        qml_file = self.symbology_path / d_regnie_qml[cpi]
        self.out(" qml: '{}'".format(qml_file))
        csv_layer.loadNamedStyle(str(qml_file))
        # Sets the opacity for the vector layer, where opacity is a value
        # between 0 (totally transparent) and 1.0 (fully opaque).
        csv_layer.setOpacity(0.6)  # 0.6 = 40% transparency
        
        regnie_max = "{} mm".format(regnie_converter.max)
        
        return csv_layer, regnie_max
        

    
    
    def product_description(self, prod_id):
        try:
            return self._product_defs[prod_id]
        except KeyError:
            return "Product has not been defined yet."
    
    
    def title(self, dt_date, prod_id):
        ''' Zugriff auf Title im Dictionary. '''
        
        s_date = dt_date.strftime("%d.%m.%Y, %H:%M UTC")
        
        return "{} des {}".format(self.product_description(prod_id), s_date)
    
    
    def qml_file(self, precision, prod_id):
        '''
        precision: 1.0, 0.1, 0.01
        prod_id:   Product-ID for determining the QML-File
        '''
        
        """
        Gemeinsame Farbskalen:
        Hierfür werden jetzt die Produkt-Summenzeiträume definiert.
        Anordnung: von klein nach groß
        """
        #min_products     = ('RZ', 'RY', 'YW', 'EZ', 'EY')    # 5 minute products
        #hourly_products  = ('RW', 'S2', 'S3')
        daily_products   = ('SF', 'D2', 'D3')
        monthly_products = ('W1', 'W2', 'W3', 'W4', 'SM')
        yearly_products  = ('SJ', 'SY', 'JW')
        
        qml_fn = 'hourly'
        
        if precision < 0.1:    # <- incl. RX RVP6 to mm conversion -> precision changed to 0.01
            qml_fn = '5min'
        elif prod_id in daily_products:
            if prod_id.startswith('D'):
                qml_fn = 'daily+'    # eine weitere Abstufung
            else:
                qml_fn = 'daily'
        elif prod_id in monthly_products:
            qml_fn = 'monthly'
        elif prod_id in yearly_products:
            qml_fn = 'yearly'
        elif prod_id[1] == 'X':    # products in RVP6-Units: RX, WX, EX
            qml_fn = 'rvp6units'
        else:
            self.out("qml_file(): using default")
        
        qml_fn += ".qml"
        qml_file = self.symbology_path / qml_fn    # beware: TypeError: unsupported operand type(s) for +: 'WindowsPath' and 'str'
        
        # QML file exists?
        if not qml_file.exists():
            self.out("qml_file(): QML file '{}' not found.".format(qml_file), False)
            qml_file = None
        
        return qml_file
    
    
    """
    Properties
    """
    
    # Dirs:
    
    @property
    def profile_dir(self):
        return QgsApplication.qgisSettingsDirPath()
    
    @property
    def plugin_dir(self):
        return self._plugin_dir    # Path
    @property
    def data_root(self):
        ''' read from definition file
        raise FileNotFoundError '''
        if self._data_root:    # cached?
            return self._data_root
        with open(self.data_root_def_file) as f:    # FileNotFoundError
            self.out("read 'data dir' from '{}':".format(f.name))
            self._data_root = Path( f.read() )
            print("  {}".format(self._data_root))
            return self._data_root
    @data_root.setter
    def data_root(self, d):
        ''' update, when user writes a new path in data dir def file '''
        self.out("update 'data_root': {}".format(d))
        self._data_root = d
    @property
    def data_dir(self):
        return self._data_dir
    @property
    def temp_dir(self):
        return self.data_root / 'tmp'    # important NOT to use '_data_root' here!
    '''
    @property
    def layout_dir(self):
        return self._plugin_dir / self._config.get('Paths', 'LAYOUT_PATH')
    '''
    @property
    def list_of_used_files(self):
        return self._list_of_used_files
    @list_of_used_files.setter
    def list_of_used_files(self, l):
        self._list_of_used_files = l
    
    @property
    def default_border_shape(self):
        """ gleich mit Check """
        #border_shape = path.join(self._plugin_dir, self._config.get('Paths', 'CUT_TO'))
        #
        #if not path.exists(border_shape):
        #    self.out("default shape mask '{}' not found!".format(border_shape), False)
        #    return None
        
        border_shape = self._plugin_dir / self._config.get('Paths', 'CUT_TO')
        
        if not border_shape.exists():
            self.out("default shape mask '{}' not found!".format(border_shape), False)
            return None
        
        return str(border_shape)    # convert as string, otherwise error "Posix path"
    
    @property
    def default_print_template(self):
        # verschiedene Namen wegen internem Pfad für Logo:
        #fn = "Druckvorlage.qpt" if not platform.system() == 'Windows'  else "Druckvorlage_win.qpt"
        
        #print_template = path.join( self._config.get('Paths', 'RESSOURCE_PATH'), "Layout/{}".format(fn) )
        #print_template = Path(self._config.get('Paths', 'RESSOURCE_PATH')) / "Layout" / fn
        
        print_template = self._plugin_dir / self._config.get('Paths', 'print_layout')
        
        #if not path.exists(print_template):
        if not print_template.exists():
            self.out("default print template '{}' not found!".format(print_template), False)
            return None
        
        return print_template
    
    @property
    def default_project_file(self):
        #project_file = self._config.get('Paths', 'DEFAULT_PROJECT')
        #project_file = path.join(self._plugin_dir, project_file)
        project_file = self._plugin_dir / self._config.get('Paths', 'DEFAULT_PROJECT')
        
        #if not path.exists(project_file):
        if not project_file.exists():
            self.out("default project '{}' not found!".format(project_file), False)
            return None
        
        return str(project_file)
    
    
    @property
    def symbology_path(self):
        #symb_path = self._config.get('Paths', 'STYLE_PATH')
        #symb_path = path.join(self._plugin_dir, symb_path)
        symb_path = self._plugin_dir / self._config.get('Paths', 'STYLE_PATH')
        
        #if not path.exists(symb_path):
        if not symb_path.exists():
            self.out("symbology path '{}' not found!".format(symb_path), False)
            return None
        
        return symb_path
    
    
    @property
    def logo_path(self):
        default_logo_image = self._plugin_dir / "img" / 'qgis_logo.png'
        
        try:
            logo_image = Path(self._config.get('Paths', 'logo_image'))
        except NoOptionError:    # use standard logo
            logo_image = default_logo_image
        else:
            if not logo_image.exists():
                self.out("logo image '{}' not found!".format(logo_image), False)
                logo_image = default_logo_image
        
        return logo_image
    
    @property
    def check_file(self):
        return Path(self.plugin_dir) / '__check__'
    @property
    def settings_file(self):
        return Path(self.plugin_dir) / 'settings.json'
    @property
    def data_root_def_file(self):
        file = Path(self.profile_dir) / self._config.get('Paths', 'datadir_deffile_basename')
        return str(file)
    @property
    def history_file(self):
        return Path(self.profile_dir) / self._config.get('Paths', 'last_products_basename')

    
    # Projections
    
    @property
    def dict_projections(self):
        return def_projections.projs
    
    @property
    def projection_radolan(self):
        _, params = def_projections.projs[0]    # [ desc, params ]
        return params
    
    

