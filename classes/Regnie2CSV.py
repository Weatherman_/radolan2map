#!/usr/bin/python3

'''
Regnie2CSV
==========
Bereitet eine originale REGNIE-Rasterdatei
zur Verwendung in einem GIS auf.
Erzeugt aus dem originären Raster eine CSV-Datei für den GIS-Input.

Aufruf des Scripts:
-------------------
(python3) Regnie2CSV.py [-a][-j] <Regnie-Rasterfile>
[] = optional
-a: alles, auch Fehlwerte rausschreiben. Führt zu einer großen Ausgabedatei.
-j: eine kleine CSV-Datei schreiben, die nur die Daten und eine ID-Spalte
    enthält. Mittels ID können die Daten mit einem Shapefile kombiniert werden.
-------------------

                        - - -

REGNIE
------
Abspeicherung der Rasterfelder:
zeilenweise von Nord nach Süd und West nach Ost.

Algorithmus:

VON 1 bis (einschl.) 971:
    VON 1 bis (einschl.) 611:
        INTEGER = READ(4 Stellen)

Nichtbesetzte Rasterpunkte sind mit dem Fehlwert -999 besetzt.
Die Dimension der monatlichen und jährlichen Niederschlagshöhen
beträgt mm, die der täglichen mm/10.

Geografischer Bezug:
Die ausgegebenen CSV-Koordinaten beziehen sich auf den Mittelpunkt
der REGNIE-Zelle.

Die Werte der monatlichen und jährlichen Niederschlagshöhen betragen mm,
die der täglichen mm/10.

CSV
---
Nichtbesetzte Rasterpunkte sind mit dem Wert -1 besetzt
(negative Werte treten sonst nicht auf).


erstellt am 29.08.2013
update: 17.01.2020
'''

from pathlib import Path
import sys
import gzip


NEWLINE_LEN = 1
"""
Standard 1
Unter Windows erstellte Textfiles (die Rasterdatei) haben idR zwei Zeichen
für einen Zeilenumbruch (\r\n).
Es wurde mit zwei Files getestet, für die allerdings die Linux-Codierung
(1 Zeichen, \n) verwendet wurde.
"""

# REGNIE-Ausdehnung:
Y_MAX = 971
X_MAX = 611



class Regnie2CSV:
    ''' Class '''
    
    
    def __init__(self, regnie_raster_file):
        ''' Constructor '''
        
        self.out("<- '{}'".format(regnie_raster_file))
        
        # Check:
        if not regnie_raster_file:
            raise FileNotFoundError("'regnie_raster_file': empty parameter!")
        
        # string -> Path:
        if isinstance(regnie_raster_file, str):
            regnie_raster_file = Path(regnie_raster_file)
        
        if not regnie_raster_file.exists():
            msg = str(regnie_raster_file)
            if not regnie_raster_file.parent.exists():    # genauerer Hinweis auf das Problem
                msg += ": File-Verzeichnis existiert nicht!"
            raise FileNotFoundError(msg)
        
        # soweit OK, Ausführung beginnen
        
        self._regnie_raster_file = regnie_raster_file
        
        
        # Ermittelt
        
        self._csv_pathname = None
        
        # Programmverhalten
        self._ignore_missings = True      # Kleinere Datei erzeugen, also Fehlwerte überspringen
        # False: alle Werte rausschreiben
        
        """ Integerwerte teilen?
        daily:   ra200107.gz    07.01.2020
        monthly: RASA1912.gz    2019-12
        
        Zehntel nur für Tagesprodukte, daher prüfen, ob 3. Zeichen im Dateinamen
        eine Zahl ist: """
        self._divide = False
        if self._regnie_raster_file.name[2].isdigit():
            self.out("Tagesprodukt erkannt - Werte 1/10")
            self._divide = True
        else:
            self.out("Monats-/Jahresprodukt erkannt")
        
        
        # Vorbereitet zur Koordinatenberechnung - muss nur ein mal berechnet werden:
        self._xdelta_grad = 1.0 /  60.0
        self._ydelta_grad = 1.0 / 120.0
        
        self._prepared_x = ( 6.0 - 10.0 * self._xdelta_grad)
        self._prepared_y = (55.0 + 10.0 * self._ydelta_grad)
        
        self._max = None    # the maximum of the field
        
        
    
    def __str__(self):
        return self.__class__.__name__
    
    
    def out(self, s, ok=True):
        if ok:
            print("{}: {}".format(self, s))
        else:
            print("{}: {}".format(self, s), file=sys.stderr)
            
        
    
    def convert(self, join_mode=False, out_dir=None):
        
        self.out("convert(join_mode={}, out_dir='{}')".format(join_mode, out_dir))
        
        
        # Ausgabe-Filenamen erzeugen:
        fn_without_ext = self._regnie_raster_file.stem
        ext = '_join.csv' if join_mode  else '_full.csv'
        new_filename = fn_without_ext + ext
        
        dest_dir = Path(out_dir) if out_dir  else self._regnie_raster_file.parent    # parent = dirname()
        self._csv_pathname = dest_dir / new_filename
        
        
        self._convert_to_csv(join_mode)
        
        self.out("Maximum: {} mm".format(self._max))
        
        
        if self._csv_pathname.exists():
            self.out("-> {}".format(self._csv_pathname))
        else:
            self.out("Fehler beim Erstellen der CSV-Datei!", False)
            


    
    def _convert_to_csv(self, join_mode):
        """
        join_mode = False (Standard):
        Vollständige CSV-Datei mit Koordinaten, einer ID und den Werten erzeugen
        
        join_mode = True:
        Erstellt eine kleine CSV-Datei, die nur die Daten und eine ID-Spalte
        enthält. Mittels ID können die Daten mit einem Shapefile kombiniert werden.
        """
        
        f_in = self._get_file_object()
        
        f_out = open(self._csv_pathname, "w")
        
        # Kopfzeile:
        if join_mode:
            print("Erzeuge CSV-Joinfile...")
            f_out.write("ID,VAL\n")
        else:
            f_out.write("LAT,LON,ID,VAL\n")
        
        
        # Beginn mit Zelle...
        _id = 1             # für Vergleich mit Regnie-Polygonen im GIS eingeführt
        
        
        # Beginn mit 1 und Ende + 1, um REGNIE-konforme Pixelindizes zu erhalten
        for y in range(1, Y_MAX+1):
            
            for x in range(1, X_MAX+1):
                
                val = int(f_in.read(4))
                
                if val < 0:   # In Ursprungs-Rasterdatei: -999
                    if self._ignore_missings:
                        continue
                    val = -1  # weiter mit diesem Wert
                elif self._divide:
                    val /= 10.0
                
                
                if join_mode:
                    f_out.write("{},{}\n".format(_id, val) )
                else:
                    point = (y,x)
                    lat, lon = self._cartesian_point_to_latlon(point)
                    f_out.write("{:f},{:f},{},{}\n".format(lat, lon, _id, val) )
                
                # note maximum:
                if val > 0:
                    self._max = val
                
                _id += 1
            
            # for x
            
            # Zeile vollständig:
            f_in.read(NEWLINE_LEN)
            print(".", end="")   # zu viele Pixel, daher nur 'for y'
            
        # for y
        
        print()
        
        f_in.close()
        f_out.close()
        
    
    
    
    def _get_file_object(self):
        if str(self._regnie_raster_file).endswith('.gz'):
            f_obj = gzip.open(self._regnie_raster_file)
        else:
            f_obj = open(self._regnie_raster_file)
        
        return f_obj
    
    
    def _cartesian_point_to_latlon(self, cartesian_point_regnie): # y, x
        """ Berechnungsfunktion """
        
        lon = self._prepared_x + (cartesian_point_regnie[1] - 1) * self._xdelta_grad
        lat = self._prepared_y - (cartesian_point_regnie[0] - 1) * self._ydelta_grad
        
        return lat, lon
    
    
    @property
    def ignore_missings(self):
        return self._ignore_missings
    @ignore_missings.setter
    def ignore_missings(self, b):
        self._ignore_missings = b
        self.out("ignore_missings = {}".format(self._ignore_missings))
        if not b:
            print("  Es wurde angegeben, dass auch nichtbelegte Werte rausgeschrieben werden (Wert -1).")
            print("  Achtung: dies führt zu einer größeren Ausgabedatei.")
    
    @property
    def csv_pathname(self):
        return self._csv_pathname
        
    @property
    def char_period_indicator(self):
        ''' for assigning a suitable QML file '''
        f = self._regnie_raster_file.name    # shorten
        
        # check in this order!
        if f[2].isdigit():
            char = 'd'    # daily, Beispiel: ra200107.gz
        elif f[3].isdigit():    # yearly, RAS6190.JAH, RAS6190.APR.gz
            char = 'y'
        else:
            char = 'm'    # monthly, RASA1908.gz
        
        return char
    
    @property
    def max(self):
        return self._max
    



def print_error_exit(msg=None):
    ''' Gibt eine übergebene Meldung aus,
    beschreibt den Aufruf des Programms
    und beendet das Programm. '''
    
    if msg:
        print(msg + "\n")
    
    print("""\
Anwendung des Scripts:
{} [-a][-j] <Regnie-Rasterfile>
[] = optional
-a: alles, auch Fehlwerte rausschreiben. Führt zu einer großen Ausgabedatei.
-j: eine kleine CSV-Datei schreiben, die nur die Daten und eine ID-Spalte enthält.
    Mittels ID-Feld können die Daten mit einem Regnie-Polygon-Shapefile kombiniert werden.
    """.format(Path(__file__).name) )
    sys.exit(1)
    


if __name__ == '__main__':
    """
    Start der Klasse
    """
    
    # Programmverhalten
    ignore_missings = True      # Kleinere Datei erzeugen, also Fehlwerte überspringen
    #ignore_missings = False    # Alle Werte rausschreiben
    """
    Es sind recht viele Fehlwerte in den Regnie-Rastern enthalten; es macht daher
    Sinn, diese Werte zu Gunsten kleinerer Ausgabedateien nicht rauszuschreiben.
    -> etwa 7,5 MB gegenüber 13 MB
    """
    
    
    """
    Argumente:
    argv[0] = Scriptname
    argv[1] = Join-Option oder Regnie-Rasterfile
    argv[2] = nicht belegt oder Regnie-Rasterfile
    """
    if len(sys.argv) < 2:
        print_error_exit()
    
    
    regnie_raster_file = None
    join_mode = False
    
    for arg in sys.argv:
        if arg == "-j":
            join_mode = True
        elif arg == "-a":
            ignore_missings = False
        else:
            regnie_raster_file = Path(arg)
    
    
    
    regnie_converter = Regnie2CSV(regnie_raster_file)    # FileNotFoundException
    regnie_converter.ignore_missings = ignore_missings
    regnie_converter.convert(join_mode)
    
    
    
