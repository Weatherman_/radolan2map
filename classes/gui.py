# -*- coding: utf-8 -*-
"""
DockWidget
-----------------
begin: 2016-08-26
last:  2019-11
"""

import os
from os import path
from pathlib import Path
import sys

from qgis.PyQt import QtGui, QtWidgets, uic
from qgis.PyQt.QtCore import pyqtSignal
from qgis.PyQt.QtWidgets import QDialogButtonBox, QFileDialog, QWidget, QListWidget, QGridLayout, QPushButton
from qgis.core import QgsProject

# nicht in 'classes', sondern direkt im Plugin-Verzeichnis:
parent_dir = path.abspath(path.join(path.dirname(__file__), os.pardir))

home_dir = str(Path.home())

"""
Widget with the plugin functionality (DockWidget):
"""
WIDGET_FORM_CLASS, _ = uic.loadUiType(path.join(parent_dir, 'dock_widget.ui'))


def get_icon(img_basename):
    ''' simplifies creating a QIcon '''
    img_full_path = path.join(parent_dir, 'img', img_basename)
    return QtGui.QIcon(img_full_path)





class DockWidget(QtWidgets.QDockWidget, WIDGET_FORM_CLASS):

    closingPlugin = pyqtSignal()
    
    def __init__(self, parent=None):
        """ Constructor. """
        super(DockWidget, self).__init__(parent)
        # Set up the user interface from Designer.
        # After setupUI you can access any designer object by doing
        # self.<objectname>, and you can use autoconnect slots - see
        # http://doc.qt.io/qt-5/designer-using-a-ui-file.html
        # #widgets-and-dialogs-with-auto-connect
        self.setupUi(self)
        
        self.setFloating(False)    # prevent losing the widget
        # -> doesn't seem to have an effect -> deselected this property in the .ui-file
        # seems that is impossible to set this as initial property in QT Creator.
        """ If you want to prevent the user from moving it to a floating window
        you need to set the "features" of the widget. In the example below,
        the widget is movable and closable, but not floatable: """
        #self.setFeatures(QtWidgets.QDockWidget.DockWidgetClosable | QtWidgets.QDockWidget.DockWidgetMovable)
        
        
        self.btn_close.clicked.connect(self.close)    # global close button
        self.btn_close.setIcon(get_icon('close.png'))
        
        
        ############################
        # Tab main action
        ############################
        
        self.tabWidget.setTabIcon(0, get_icon('execute.png'))
        # set toolbar button icons:
        self.btn_load_project.setIcon(get_icon('new.png'))
        self.btn_load_radars.setIcon(get_icon('radar.png'))
        
        #self.btn_show_list.clicked.connect(self._show_list)
        #self.btn_show_list.setVisible(False)    # first select directory
        
        self.widget_symb.setVisible(False)
        # Only enabled for RX products (check at every load):
        self.check_rvp6tomm.setVisible(False)
        
        # connect functions:
        #self.btn_info.clicked.connect(self.open_about_dialog)
        self.filedialog_mask.clicked.connect(self.select_mask)
        # passing parameters to connected method only possible with keyword 'lambda':
        self.check_cut.stateChanged.connect(lambda:self.checkbox_state_changed(self.check_cut))
        self.check_symb.stateChanged.connect(lambda:self.checkbox_state_changed(self.check_symb))
        self.check_rvp6tomm.stateChanged.connect(lambda:self.checkbox_state_changed(self.check_rvp6tomm))
        
        #if not self.dock.inputpath.text():
        #    #self.dock.button_box.button(QDialogButtonBox.Cancel).setEnabled(True)
        #    self.dock.button_box.button(QDialogButtonBox.Apply).setEnabled(False)
        #    #self.out("OK button disabled -> please load a RADOLAN binary file first!")
        self.btn_action.setIcon(get_icon('execute.png'))
        self.btn_action.setEnabled(False)    # initially disabled, need to load RADOLAN file
        
        # trigger deactivating clipping:
        self.check_cut.setChecked(False)
        self.checkbox_state_changed(self.check_cut)
        
        
        ############################
        # Tab Statistics
        ############################
        self.tabWidget.setTabEnabled(1, False)    # second tab "statistics"
        self.tabWidget.setTabIcon(1, get_icon('stats.png'))
        
        
        ############################
        # Tab TIF storage
        ############################
        
        self.tabWidget.setTabIcon(2, get_icon('folder.png'))
        
        self.btn_select_storage_dir.setIcon(get_icon('folder.png'))
        self.btn_select_storage_dir.clicked.connect(self._select_storage_dir)
        
        self.btn_save.setIcon(get_icon('save.png'))
        # save button is disabled by default
        self.btn_save.setEnabled(False)
        
        #self.setWindowIcon(get_icon('folder.png'))
        
        
        ############################
        # Tab REGNIE
        ############################
        
        self.tabWidget.setTabIcon(3, get_icon('regnie.png'))
        self.btn_select_regnie.setIcon(get_icon('folder.png'))
        self.btn_load_regnie.setIcon(get_icon('regnie.png'))
        
        
        
        ############################
        # Tab "about"
        ############################
        
        self.tabWidget.setTabIcon(4, get_icon('info.png'))
        
        # insert images:
        logo_path = Path(parent_dir) / 'img' / 'plugin_logo.png'
        self.label_logo.setPixmap(QtGui.QPixmap(str(logo_path)))
        
        img = Path(parent_dir) / 'img' / 'sw_info.png'
        self.label_img_info.setPixmap(QtGui.QPixmap(str(img)))
        
        img = Path(parent_dir) / 'img' / 'sw_download.png'
        self.label_img_download.setPixmap(QtGui.QPixmap(str(img)))
        
        # fill text fields with metadata:
        self._fill_fields()
        
        ############################
        
        #self.list_widget = FileList()
        
    
    
    def __str__(self):
        return self.__class__.__name__
    
    def out(self, s, ok=True):
        if ok:
            print("{}: {}".format(self, s))
        else:
            print("{}: {}".format(self, s), file=sys.stderr)
    
    
    
    def checkbox_state_changed(self, checkbox):
        name = checkbox.objectName()
        b = checkbox.isChecked()
        
        # Diag:
        #self.out("checkbox_state_changed() from '{}': {}".format(name, b))
        
        if name == 'check_cut':
            self.inputmask.setEnabled(b)
            self.filedialog_mask.setEnabled(b)
        elif name == 'check_symb':
            self.widget_symb.setVisible(b)
    
    
    def select_mask(self):
        start_dir = self.inputmask.text()
        # We assume a set up file here:
        if start_dir:
            start_dir = path.dirname(start_dir)
        else:
            start_dir = home_dir
        
        # parent, caption, directory, file_filter
        inputmaskshp, _ = QFileDialog.getOpenFileName(self, "Select mask (optional)",
                                                      start_dir, "Shape file (*.shp)",
                        # these additional parameters are used, because QFileDialog otherwise doesn't start with the given path:
                        None, QFileDialog.DontUseNativeDialog)
        if not inputmaskshp:
            return    # evtl. geladener Pfad nicht löschen
        self.inputmask.setText(inputmaskshp)
    
    
    def _select_storage_dir(self):
        start_dir = self.textfield_path.text()
        # We assume a set up file here:
        if not start_dir:
            start_dir = home_dir
        
        # parent, caption, directory, file_filter
        selected_dir = QFileDialog.getExistingDirectory(self, 'Select RADOLAN/RADKLIM directory', start_dir,
                        # these additional parameters are used, because QFileDialog otherwise doesn't start with the given path:
                        QFileDialog.DontUseNativeDialog)
        
        if not selected_dir:
            return    # preserve evtl. filled line
        
        # NOT 'radolan2map/radolan2map'!
        const_last_part = 'radolan2map'
        if not selected_dir.endswith(const_last_part):
            complete_path = path.join(selected_dir, const_last_part)
        else:
            complete_path = selected_dir
        
        # Set:
        self.textfield_path.setText(complete_path)
        
        # after folder selection enable save button:
        self.btn_save.setEnabled(True)
    
    
    
    '''
    def _show_list(self):
        if self.list_widget.isVisible():
            self.list_widget.close()
        else:
            self.list_widget.show()
    '''
    
    def closeEvent(self, event):
        self.closingPlugin.emit()
        event.accept()

    
    
    def _fill_fields(self):
        metadata_file = path.join(parent_dir, 'metadata.txt')
        
        version       = "?"
        issue_tracker = "?"
        mail_link     = "?"
        
        self.out("reading '{}'".format(metadata_file))
        
        with open(metadata_file) as f:
            for line in f:
                """ filter lines from metadata file:
                version=0.6
                email=radolan2map@e.mail.de
                tracker=https://gitlab.com/Weatherman_/radolan2map/issues
                """
                
                if line.startswith('version'):
                    version = self.__get_value(line)
                elif line.startswith('email'):
                    mailadress = self.__get_value(line)
                    mail_link= '<a href="mailto:{}">{}</a>'.format(mailadress, mailadress)
                elif line.startswith('tracker'):
                    issue_link = self.__get_value(line)
                    issue_tracker = '<a href="{}">{}</a>'.format(issue_link, issue_link)
            # for
        # with
        
        self.text_version.setText(version)
        self.text_issue.setText(issue_tracker)
        self.text_mailaddress.setText(mail_link)
        
    def __get_value(self, line):
        return line.strip().split('=')[1]    # version=0.6
    
    
    
    @property
    def storage_dir(self):
        return self.textfield_path.text()
    @storage_dir.setter
    def storage_dir(self, d):
        self.textfield_path.setText(d)
    
    


class FileList(QWidget):
    def __init__(self):
        QWidget.__init__(self)
        self.setWindowTitle('Select a file')
        
        self.listWidget = QListWidget()
        self.listWidget.clicked.connect(self.clicked)
        
        self.btn_close = QPushButton("Close")
        self.btn_close.clicked.connect(self.close)
        
        layout = QGridLayout()
        layout.addWidget(self.listWidget)
        layout.addWidget(self.btn_close)
        
        self.setLayout(layout)
        
    def __str__(self):
        return self.__class__.__name__
    
    def out(self, s, ok=True):
        if ok:
            print("{}: {}".format(self, s))
        else:
            print("{}: {}".format(self, s), file=sys.stderr)
    
    def clicked(self):
        item = self.listWidget.currentItem()
        print(item.text())
    
    def add_items(self, l_items):
        self.out("load {} files".format(len(l_items)))
        self.listWidget.clear()
        self.listWidget.addItems(l_items)


"""
if __name__ == "__main__":
    
    dlg = AboutDialog()
    dlg.show()
"""
