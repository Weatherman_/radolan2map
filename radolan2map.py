# -*- coding: utf-8 -*-
"""
RADOLAN to map (radolan2map)

A QGIS plugin to bring a RADOLAN binary file onto a map.

changed: 2019-09
email  : radolan2map@e.mail.de

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""

import os
from os import path       # shortcut
from pathlib import Path
import sys
#import platform    # to determine wether Windows or not
import time
import json

# Import the PyQt and QGIS libraries
from qgis.PyQt.QtCore import Qt, QCoreApplication, QSettings, QFileInfo, QTranslator, QSize, pyqtSignal #, QRect
from qgis.PyQt.QtWidgets import QMessageBox, QDockWidget, QAction, QFileDialog, QDialogButtonBox #, QDialog
from qgis.PyQt.QtGui import QIcon

from qgis.core import Qgis, QgsProject, QgsLayoutManager, QgsPrintLayout, QgsLayerTreeGroup
from qgis.core import QgsVectorLayer, QgsRasterLayer, QgsMapLayer
from qgis.core import QgsReadWriteContext, QgsLayoutItemLegend, QgsLayerTree, QgsLayerTreeLayer, QgsRasterTransparency
#from qgis.core import QgsCoordinateReferenceSystem

# Initialize Qt resources from file resources.py
#from .resources import *

from console import console    # Python-Konsole automatisch zeigen

# Eigene Klassen
# .........................................................
class_path = path.join(path.dirname(__file__), 'classes')
sys.path.append(class_path)    # Verzeichnis der weiteren Klassen
from gui import DockWidget
from Model import Model
from RadolanBin2AsciiConverter import RadolanBin2AsciiConverter
from RadolanBin2AsciiConverter import test_product_get_id    # import a function
from GDALProcessing import GDALProcessing
# .........................................................



class Radolan2Map:
    """ QGIS Plugin Implementation. """
    
    def __init__(self, iface):
        """Constructor.
        :param iface: An interface instance that will be passed to this class
            which provides the hook by which you can manipulate the QGIS
            application at run time.
        :type iface: QgsInterface
        """
        
        # Save reference to the QGIS interface
        self.iface = iface
        
        
        # initialize plugin directory
        self.plugin_dir = path.dirname(__file__)
        
        # initialize locale
        # Bugfix: on Windows 10 and QGIS 3.6 Noosa occured a TypeError.
        # The reason could be, that there are no translation files.
        try:
            locale = QSettings().value('locale/userLocale')[0:2]
        except TypeError as e:
            self.out(e, False)    # TypeError: 'QVariant' object is not subscriptable
        else:
            locale_path = path.join(
                self.plugin_dir,
                'i18n',
                'Radolan2Map_{}.qm'.format(locale))
            
            if path.exists(locale_path):
                self.translator = QTranslator()
                self.translator.load(locale_path)
                QCoreApplication.installTranslator(self.translator)
        
        # Declare instance attributes
        self.actions = []
        self.menu = self.tr(u'&radolan2map')
        # TODO: We are going to let the user set this up in a future iteration
        self.toolbar = self.iface.addToolBar(u'Radolan2Map')
        self.toolbar.setObjectName(u'Radolan2Map')

        
        self._model = Model()
        
        # RADOLAN-Converter bin -> asc:
        self._ascii_converter = None
        
        # Check if plugin was started the first time in current QGIS session
        # Must be set in initGui() to survive plugin reloads
        # Create the dialog (after translation) and keep reference
        self.dock = None
        
        self._l_projections = []    # EPSG-Numbers or projection parameters
        
        
        
        
    
    
    def __str__(self):
        return self.__class__.__name__
    
    
    def out(self, s, ok=True):
        if ok:
            print("{}: {}".format(self, s))
        else:
            print("{}: {}".format(self, s), file=sys.stderr)
    
    
    
    # noinspection PyMethodMayBeStatic
    def tr(self, message):
        """Get the translation for a string using Qt translation API.

        We implement this ourselves since we do not inherit QObject.

        :param message: String for translation.
        :type message: str, QString

        :returns: Translated version of message.
        :rtype: QString
        """
        # noinspection PyTypeChecker,PyArgumentList,PyCallByClass
        return QCoreApplication.translate('Radolan2Map', message)
    
    def add_action(
        self,
        icon_path,
        text,
        callback,
        enabled_flag=True,
        add_to_menu=True,
        add_to_toolbar=True,
        status_tip=None,
        whats_this=None,
        parent=None):
        """Add a toolbar icon to the toolbar. """

        icon = QIcon(icon_path)
        action = QAction(icon, text, parent)
        action.triggered.connect(callback)
        action.setEnabled(enabled_flag)

        if status_tip is not None:
            action.setStatusTip(status_tip)

        if whats_this is not None:
            action.setWhatsThis(whats_this)

        if add_to_toolbar:
            self.toolbar.addAction(action)

        if add_to_menu:
            self.iface.addPluginToRasterMenu(
                self.menu,
                action)

        self.actions.append(action)
        
        return action
    
    
    
    def initGui(self):
        """ Create the menu entries and toolbar icons inside the QGIS GUI. """
        
        
        """
        note the icon path:
        ':/plugins/my_plugin/icon.png'.
        The colon instructs QGIS to use the compiled resources.py file to locate the icon.
        Open the original resources.qrc file and you'll see the connection.
        """
        #icon_path = ':/plugins/radolan2map/icon.png'
        icon_path = path.join(path.dirname(__file__), "img/icon.png")
        
        self.add_action(
            icon_path,
            text=self.tr(u'radolan2map: load a RADOLAN binary file'),    # tooltip over plugin icon
            callback=self.open_dock,
            parent=self.iface.mainWindow())
    

    
    def onClosePlugin(self):
        """ Cleanup necessary items here when plugin dockwidget is closed """
        
        # disconnects
        self.dock.closingPlugin.disconnect(self.onClosePlugin)

        # remove this statement if dockwidget is to remain
        # for reuse if plugin is reopened
        # Commented next statement since it causes QGIS crashe
        # when closing the docked window:
        # self.dock = None
    
    
    def unload(self):
        """
        Removes the plugin menu item and icon from QGIS GUI.
        => Executed when exit QGIS - also when plugin never opened!
        """
        
        for action in self.actions:
            self.iface.removePluginRasterMenu(
                self.tr(u'&radolan2map'),
                action)
            self.iface.removeToolBarIcon(action)
        # remove the toolbar
        del self.toolbar
        
        
        # Plugin (Dock) never initialized - nothing to do - otherwise access to None-types:
        if not self.dock:
            return
        
        #
        #        Save
        #
        """
        save used files for suggestion to the user at next start
        """
        self._model.write_history_file()
        
        
        """
        save program settings
        """
        # fetch attributes for saving to file now:
        settings = {}
        check_text = self.dock.inputmask.text()
        if check_text:
            settings['mask_file'] = check_text
        check_text = self.dock.inputqml.text()
        if check_text:
            settings['qml_file']  = check_text
        settings['bool_cut']         = self.dock.check_cut.isChecked()
        settings['bool_excl_zeroes'] = self.dock.check_excl_zeroes.isChecked()
        settings['bool_symb']        = self.dock.check_symb.isChecked()
        
        check_text = self.dock.text_regnie.text()
        if check_text:
            settings['regnie'] = check_text
        
        with open(self._model.settings_file, 'w') as json_file:
            json.dump(settings, json_file, indent=4)
    
    
    
    def select_input_file(self):
        text_input_field = self.dock.cbbox_radolan.currentText()
        
        # Determine start directory:
        # We assume a set up file here.
        if text_input_field:
            file = text_input_field
        else:
            file = QgsProject.instance().fileName()    # project file
        
        start_dir = path.dirname(file)
        
        """
        raa01-rw_10000-1708020250-dwd---bin
        raa01-rw_10000-1708020250-dwd---bin.gz
        adjust.pix """
        # You can also add other filter. You need to separate them with a double ;; like so :
        # "Images (*.png *.xpm .jpg);;Text files (.txt);;XML files (*.xml)"
        file_filter = "RADOLAN binary files (*bin* *.pix);;Unknown product format (*)"
        
        input_file, _ = QFileDialog.getOpenFileName(self.dock, "Input binary RADOLAN file", start_dir, file_filter,
                        # these additional parameters are used, because QFileDialog otherwise doesn't start with the given path:
                        None, QFileDialog.DontUseNativeDialog)
        
        if not input_file:
            return    # preserve evtl. filled line
        
        # Set:
        
        #self.dock.inputpath.setText(input_file)
        combo = self.dock.cbbox_radolan    # shorten
        
        """ Set selection to first (just inserted) position.
        This triggers the change event listener which is then executed twice.
        So we block the emitting of a signal during index change: """
        combo.blockSignals(True)
        combo.insertItem(0, input_file)    #combo.addItem(input_file)    # addItem simplest method
        combo.blockSignals(False)
        combo.setCurrentIndex(0)    # trigger if there is a real selection change (more than one element)
        
        # If one element now, the event listener was not triggered,
        # because there is no selection change. We trigger manually:
        if combo.count() == 1:
            self._combo_selection_change()
        
        
    
    def _combo_selection_change(self):
        #file = self.dock.inputpath.text()
        file = self.dock.cbbox_radolan.currentText()
        self.out("selection changed: '{}'".format(file))
        
        #
        # Check input file:
        #
        
        action_btn = self.dock.btn_action    # shorten
        
        if not file:
            print("  empty entry ignored")
            action_btn.setEnabled(False)
            return
        
        if not Path(file).exists():
            QMessageBox.critical(self.iface.mainWindow(),
                'File error', "The specified file doesn't exist!")
            self._model.list_of_used_files.remove(file)
            self.out("file '{}' removed from list".format(file))
            action_btn.setEnabled(False)
            return
        
        # so far OK, ebenable action
        
        # Test product, if it is a 'X'-product (RX, WX, EX) with values coded as RVP6-units:
        prod_id = test_product_get_id(file)
        
        is_rx = True if prod_id[1] == 'X'  else False
        """ Further string tests on product id don't make sense,
        because IDs like 'W1'-'W4', 'D2'-'D3', 'S2'-'S3' are possible. """
        
        if is_rx:
            self.out("*X product detected - '{}' checkbox enabled".format(self.dock.check_rvp6tomm.objectName()))
        self.dock.check_rvp6tomm.setVisible(is_rx)
        
        # Print info found in def-file about the product:
        prod_desc = self._model.product_description(prod_id)
        self.out('product {}: "{}"'.format(prod_id, prod_desc))
        
        #self.dock.btn_show_list.setVisible(True)
        # after selection of RADOLAN file enable action button:
        action_btn.setEnabled(True)
        
    
    
    
    def open_dock(self):
        """
        This method will be called when you click the toolbar button or select the plugin menu item.
        """
        
        # Try to catch every Exception and show it in a graphical window.
        try:
            
            
            # Ausgaben des Plugins zeigen:
            #console.show_console()    # geht, aber besser:
            pythonConsole = self.iface.mainWindow().findChild( QDockWidget, 'PythonConsole' )
            try:
                if not pythonConsole.isVisible():
                    pythonConsole.setVisible(True)
            except AttributeError:    # can be 'None' above
                console.show_console()
                self.out("PythonConsole Error catched", False)
            
            
            self.out("open_dock()")
            
            
            # Test for catching a exception and show this to the user by a window:
            #raise RuntimeWarning("manual exception raised")
            
            
            # Create the dialog with elements (after translation) and keep reference
            # Only create GUI ONCE in callback, so that it will only load when the plugin is started
            
            # dockwidget may not exist if:
            #    first run of plugin
            #    removed on close (see self.onClosePlugin method)
            
            
            # Close dock via toolbar button:
            if self.dock:
                if self.dock.isVisible():
                    self.dock.close()
                    return
            # if first start
            else:
                """
                Detect first start after installation.
                There seems to be a problem with plugin reloading, so after
                (re)install there occur errors. So we handle this issue manually with
                a one time check file (deleted after the following message).
                """
                if self._model.check_file.exists():
                    msg = "After reinstalling the plugin\nQGIS should be restarted now,"\
                    " otherwise plugin errors are to be expected!"
                    QMessageBox.warning(self.iface.mainWindow(), 'initialize plugin', msg)
                    # Message should only appear one time, so the
                    # check file must be removed now:
                    self._model.check_file.unlink()
                    self.out("check file '{}' removed, message doesn't appear again.".format(self._model.check_file))
                # if
                self._init_dock()
            
            
            """
            DockWidget exists but maybe it's invisible
            Bring DockWidget to front (maybe it's minimized elsewhere...)
            """
            width  = self.dock.frameGeometry().width()
            height = self.dock.frameGeometry().height()
            # das ist schon gut - ggf. die init. Werte vom .ui holen
            self.dock.setMinimumSize(QSize(width, height))    # width, height
            # -> verhindert ein zu kleines Widget
            
            
            # determine projection (EPSG code) of current project:
            if QgsProject.instance().fileName():    # if project loaded:
                epsg_code = self.iface.mapCanvas().mapSettings().destinationCrs().authid()
                # Problem occured on Windows 10 with QGIS 3.10: empty 'epsg_code'.
                # Maybe with the application of "setDestinationCrs()" in 'create_default_project()' the problem isn't existent anymore.
                if not epsg_code:
                    self.out("project CRS couldn't be determined; this is a unknown problem on Windows"
                             + " - not critical", False)
                # add only, when this projection doesn't exists:
                elif not epsg_code in self._l_projections:
                    projection_description = "Project: {}".format(epsg_code)
                    self._add_projection(projection_description, epsg_code)
                    # projection appended last, select last entry:
                    index = len(self._l_projections) - 1    # -1 as last index doesn't work!
                    self.dock.cbbox_projections.setCurrentIndex(index)
            
            
            
            # show the dockwidget
            self.iface.addDockWidget(Qt.RightDockWidgetArea, self.dock)
            self.dock.show()
            # Ohne diese Anweisung wurde das Fenster ab QGIS3 im Hintergrund geöffnet.
            #self.dock.setWindowFlags(Qt.WindowStaysOnTopHint)
            
            
        except Exception as e:
            l_msg = ["{}\n\n".format(e)]
            l_msg.append("If this error persists - even after restarting QGIS - it may be")
            l_msg.append(" helpful to update your QGIS / Python installation")
            l_msg.append(" and working with a new QGIS user profile.")
            l_msg.append("\nIf nothing helps, please write a bug report.")
            
            msg = "".join(l_msg)
            self.out(msg, False)
            QMessageBox.critical(self.iface.mainWindow(), 'Exception catched', msg)
        
        
        
    
    def _init_dock(self):
        '''
        Everything that is neccessary for setup up the DockWidget. 
        '''
        
        # Create the dockwidget (after translation) and keep reference
        self.dock = DockWidget()
        
        # connect to provide cleanup on closing of dockwidget
        self.dock.closingPlugin.connect(self.onClosePlugin)
        
        
        """
        set connections
        """
        
        # Button for loading a prepared template project:
        self.dock.btn_load_project.clicked.connect(self.ask_user_load_template_project)
        # Button für schnellen Test des PrintLayouts nutzen:
        #self.dock.btn_load_project.clicked.connect(self._load_print_layout )    # !: Funktion ohne ()
        self.dock.btn_load_radars.clicked.connect(self._model.load_radarnetwork_layer)
        self.dock.filedialog_input.clicked.connect(self.select_input_file)
        
        self.dock.filedialog_qml.clicked.connect(self.select_symbology)
        # connect accept and cancel:
        #self.dock.button_box.accepted.connect(self._run)
        # For Apply-Button you'll need to connect the clicked signal manually in your widget:
        #apply_btn = self.dock.button_box.button(QDialogButtonBox.Apply)
        #apply_btn.clicked.connect(self._run)
        self.dock.btn_action.clicked.connect(self._run)
        
        
        """
        resets / defaults
        """
        
        ############################
        # Tab action
        ############################
        
        combo = self.dock.cbbox_radolan    # shorten
        
        # read previously used files and prefill combo box for the user:
        history_file = self._model.history_file
        if history_file.exists():
            self.out("reading 'history file' {}".format(history_file))
            with open(history_file) as hf:
                self._model.list_of_used_files = hf.read().splitlines()    # lines without '\n'
                # hint: 'list_of_used_files' will be used further
            # fill:
            combo.addItem('')    # first a empty item (no valid selection)
            for file in self._model.list_of_used_files:    # first items in the file also positioned first in list
                combo.addItem(file)
        # history file
        
        # Register event handler only here, so it is not triggered at filling above:
        combo.currentIndexChanged.connect(self._combo_selection_change)
        
        
        ############################
        # Tab TIF storage / Projection
        ############################
        
        self.dock.btn_save.clicked.connect(self.write_new_storage_location)
        # Try to show the existing data folder, but maybe the file does not exist yet:
        try:
            self.dock.storage_dir = str(self._model.data_root)
        except FileNotFoundError:
            pass
        
        
        ############################
        # Tab REGNIE
        ############################
        self.dock.btn_select_regnie.clicked.connect(self.select_regnie_file)
        
        
        """
        fill projections
        Qt-element is connected with the projection list, via same index
        """
        
        for number, proj_desc in self._model.dict_projections.items():
            if number > 999:    # 4 digits expected
                entry = proj_desc
                projection = 'EPSG:{}'.format(number)
            else: # !: list type expected
                assert type(proj_desc) is list
                entry, projection = proj_desc
            
            self._add_projection(entry, projection)
        
        
        """
        If data def file doesn't exist, the action tab will be disabled
        """
        tab_to_activate_index = 0    # 0 ...
        if path.exists(self._model.data_root_def_file):
            self.out("'data dir def file' found:\n  {}".format(self._model.data_root_def_file))
        else:
            self.out("'data dir def file' not found -> init")
            #self.dock.tabWidget.removeTab(0)    # -> working with the plugin is not possible
            self.dock.tabWidget.setTabEnabled(tab_to_activate_index, False)    # disable first tab "operation"
            self.dock.tabWidget.setTabEnabled(3, False)    # disable also tab "REGNIE"
            tab_to_activate_index = 2    # tab for define storage folder
        
        self.dock.tabWidget.setCurrentIndex(tab_to_activate_index)
        
        
        """
        Load settings from file
        """
        
        settings_file = self._model.settings_file
        mask_file = self._model.default_border_shape    # default: "DEU_adm0"
        
        if settings_file.exists():
            self.out("reading settings from '{}'".format(settings_file))
            with open(settings_file) as json_file:
                settings = json.load(json_file)
                
                try:
                    mask_file = settings['mask_file']
                except KeyError:
                    pass
                try:
                    self.dock.inputqml.setText(settings['qml_file'])
                except KeyError:
                    pass
                # always available:
                self.dock.check_cut.setChecked(settings['bool_cut'])
                self.dock.check_excl_zeroes.setChecked(settings['bool_excl_zeroes'])
                self.dock.check_symb.setChecked(settings['bool_symb'])
                
                try:
                    self.dock.text_regnie.setText(settings['regnie'])
                except KeyError:
                    pass
                else:
                    self.dock.btn_load_regnie.setEnabled(True)
                
        else:
            self.out("settings file '{}' doesn't exist yet.".format(settings_file))
        # if settings
        
        self.dock.inputmask.setText(mask_file)
        
        
        self.dock.btn_load_regnie.clicked.connect(self.load_regnie)
        
    
    
    
    def select_symbology(self):
        file_filter = "QGIS symbology files (*.qml*)"
        
        qml_file, _ = QFileDialog.getOpenFileName(self.dock, "Apply symbology (optional)",
                                                       str(self._model.symbology_path), file_filter,
        # these additional parameters are used, because QFileDialog otherwise doesn't start with the given path:
                                                       None, QFileDialog.DontUseNativeDialog)
        if not qml_file:
            return    # keep path anyway
        
        self.dock.inputqml.setText(qml_file)
        
    
    
    def select_regnie_file(self):
        start_dir = None
        text = self.dock.text_regnie.text()
        if text:
            start_dir = str( Path(text).parent )
        
        # after title string: start path, file_filter
        regnie_file, _ = QFileDialog.getOpenFileName(self.dock, "Please select a REGNIE raster file",
                                                       start_dir, None,
        # these additional parameters are used, because QFileDialog otherwise doesn't start with the given path:
                                                       None, QFileDialog.DontUseNativeDialog)
        if not regnie_file:
            return    # keep path anyway
        
        self.dock.text_regnie.setText(regnie_file)
        self.dock.btn_load_regnie.setEnabled(True)
    
    
    
    
    def _run(self):
        """
        Run method that performs all the real work.
        """
        
        model = self._model    # shorten
        
        
        file = self.dock.cbbox_radolan.currentText()
        
        # File exists (checked before) - so it can be already stored for later suggestion for the user:
        
        
        # Try to remove _this_ file (if contains), so it can be new positioned at the top:
        try:
            model.list_of_used_files.remove(file)
        except ValueError:
            pass
        
        model.list_of_used_files.insert(0, file)
        
        
        
        clip_to_mask = False
        shapefile = self.dock.inputmask.text()
        # Checkbox enabled AND mask shape specified?
        if self.dock.check_cut.isChecked()  and  shapefile:
            clip_to_mask = True
            #sep = '\\' if platform.system() == 'Windows'  else '/'
            #shapefile = shapefile.replace(sep, os.sep)
            
            # if the use of mask file will be relevant, check it
            if not Path(shapefile).exists():
                QMessageBox.critical(self.iface.mainWindow(),
                    'File error', "The specified mask file doesn't exist!")
                return
        
        
        # + + + + + + + + + + + + + + + +
        
        
        
        model.create_storage_folder_structure('temp_dir')    # only create tmp_dir
        
        
        #exclude_zeroes = self.dock.check_excl_zeroes.isChecked()
        #self._ascii_converter = RadolanBin2AsciiConverter(input_file, model.temp_dir, exclude_zeroes)
        # -> now better with layer renderer - conserves 0 values
        self._ascii_converter = RadolanBin2AsciiConverter(file, model.temp_dir)
        
        # RX to mm?
        if self.dock.check_rvp6tomm.isVisible()  and  self.dock.check_rvp6tomm.isChecked():
            self._ascii_converter.rvp_to_mm = True
        
        try:
            self._ascii_converter.run()    # ggf. Exception (from NumpyRadolanReader)
        except Exception as e:    # catch everything
            self.out(e, False)
            QMessageBox.critical(self.iface.mainWindow(), 'Problem reading RADOLAN bin file occured', str(e))
            return
        
        #prod_id = self._ascii_converter.prod_id
        
        filename, dim, _max, _min, mean, total, valid, nonvalid = self._ascii_converter.statistics
        s_max = str(_max)
        # if part after point is to long:
        l_max = s_max.split('.')
        if len(l_max[1]) > 2:
            s_max = "{:.2f}".format(_max)
        
        self.dock.text_filename.setText(filename)
        self.dock.text_shape.setText(dim)
        self.dock.text_max.setText(s_max)
        self.dock.text_min.setText(str(_min))
        self.dock.text_mean.setText("{:.2f}".format(mean))
        self.dock.text_total_pixels.setText(str(total))
        self.dock.text_valid_pixels.setText(str(valid))
        self.dock.text_nonvalid_pixels.setText(str(nonvalid))
        
        self.dock.tabWidget.setTabEnabled(1, True)    # second tab "statistics", 0 ...
        
        
        # -> .asc-Name gesetzt
        # + + + + + + + + + + + + + + + +
        
        
        # define name of (clipped) TIFF file (based on .asc filename):
        tif_extension = '_clipped.tif' if clip_to_mask  else '.tif'
        
        full_asc_filename = self._ascii_converter.full_asc_filename
        tif_bn = path.basename(full_asc_filename).replace('.asc', tif_extension)    # tif basename
        
        subdir_name = 'radklim' if self._ascii_converter.is_radklim  else 'radolan'
        
        model.set_data_dir(subdir_name)
        model.create_storage_folder_structure()    # no cleaning temp, so we can check the temp result after running
        
        
        full_tif_filename = str( model.data_dir / tif_bn)    # string type important! otherwise problems with gdal.Warp()!
        
        
        gdal_processing = GDALProcessing(model, full_asc_filename, full_tif_filename)
        #gdal_processing.produce_warped_tif_using_script()
        
        
        
        # Clip to the given mask (optional):
        if not clip_to_mask:
            shapefile = None
        
        #l_elems = self.dock.cbbox_projections.currentText().split()    # EPSG:3035 ETRS89 / LAEA Europe
        #epsg_code = l_elems[0]
        #if epsg_code == '-':    # RADOLAN
        #    epsg_code = model.projection_radolan    # complete RADOLAN projection parameters
        prj_src = model.projection_radolan
        index = self.dock.cbbox_projections.currentIndex()
        prj_dest = self._l_projections[index]
        
        # at GDAL processing a lot of strange errors are possible - with projection parameters and GDAL versions...
        try:
            gdal_processing.produce_warped_tif_by_python_gdal(prj_src, prj_dest, shapefile)
        except Exception as e:
            QMessageBox.critical(self.iface.mainWindow(), 'GDAL processing error', str(e))
            return
        
        """
        From here is related to GIS layer loading
        """
        
        raster_to_load = gdal_processing.tif_file
        
        
        result_layer = QgsRasterLayer(raster_to_load, path.basename(raster_to_load) )
        
        # this too? Lead to layer loaded twice.
        #QgsMapLayerRegistry.instance().addMapLayer(result_layer)
        
        if not result_layer.isValid():
            msg = "Layer '{}' failed to load!".format(path.basename(raster_to_load))
            self.out(msg, False )
            self.iface.messageBar().pushMessage("Error", msg, level=Qgis.Critical, duration=3)
            return
        
        """
        Das neue Rasterlayer ist i.O. und es wird im Folgenden hier weiter damit gearbeitet
        """
        
        
        radolan_layer_name = self._ascii_converter.layer_name
        if clip_to_mask:
            radolan_layer_name += "_clipped"
        
        
        """
        --- Clean: Ggf. vorhandenes Raster entfernen ---
        
        Wenn schon ein Rasterlayer mit obigem Namen existiert, wird dieses zuvor entfernt.
        Die "Legende" hat hier wohl nichts mit der Composer-Legende zu tun!
        """
        
        #for layer in self.iface.legendInterface().layers():    # QGIS 2
        #for layer in self.iface.layerTreeView().selectedLayers():
        raster_layers = [rl for rl in QgsProject.instance().mapLayers().values() if rl.type() == 1]
        self.out("current project: {} raster layers found.".format(len(raster_layers)))
        
        for layer in raster_layers:
            #if layer.type() == QgsMapLayer.RasterLayer  and  layer.name() == radolan_layer_name:
            if layer.name() == radolan_layer_name:
                self.out('RasterLayer with existing name "{}" found.'.format(layer.name()))
                """ Ausgabe
                1 Raster
                0 DEU_adm0
                """
                #print(layerType, layer.name())
                
                print("  removing layer \"{}\"".format(layer.name()))
                QgsProject.instance().removeMapLayer( layer.id() )
        # for
        
        
        #
        # Load result raster
        #
        
        # Vorherige Version ohne Insert an bestimmter Position (ging auch):
        #self.iface.addRasterLayer(raster_to_load, path.basename(raster_to_load) )
        
        # Einfaches result_layer.setLayerName('Raster') bringt leider nichts.
        # Unser Rasterlayer kann zuverlässig so bestimmt werden (kann gleich noch einmal gebraucht werden):
        #active_raster_layer = self.iface.activeLayer()
        active_raster_layer = result_layer
        active_raster_layer.setName(radolan_layer_name)
        
        
        
        """
        If no project file not loaded when running plugin
        """
        
        project = QgsProject.instance()
        # Print the current project file name (might be empty in case no projects have been loaded)
        #self.out("bisheriges Projekt: {}".format(project.fileName()))
        
        write = False
        
        if not path.exists(project.fileName()):    # prepare project
            self.out("### no project open -> create a new one ###")
            project = model.create_default_project()
            write = True
        
        
        # Insert layer at a certain position
        
        # Add the layer to the QGIS Map Layer Registry (the second argument must be set to False
        # to specify a custom position:
        project.addMapLayer(active_raster_layer, False)  # first add the layer without showing it
        
        # obtain the layer tree of the top-level group in the project
        layerTree = self.iface.layerTreeCanvasBridge().rootGroup()
        # The position is a number starting from 0, with -1 an alias for the end.
        # Index 0: ganz oben, Index 1: 2. Position unter der Vektor-Layer-Gruppe:
        layerTree.insertChildNode(1, QgsLayerTreeLayer(active_raster_layer))
        
        
        
        # Set symbology from QML file:
        # 1) as given parameter by user or
        # 2) automatically by program
        qml_file = None
        
        # self defined symbology:
        if self.dock.check_symb.isChecked():
            qml_file = self.dock.inputqml.text()
            if not qml_file:    # empty field (string)?
                self.out("QML field = emtpy - choose one automatically", False)
            
        # Determine prepared QML-File delivered with the plugin:
        if not qml_file:
            qml_file = model.qml_file(self._ascii_converter.precision, self._ascii_converter.prod_id)
            # <prod_id>.qml or 'None'
        
        if qml_file:
            self.out("using QML file '{}'".format(qml_file))
            
            # strange, this doesn't work:
            #result_layer.loadNamedStyle(qml_file)
            
            #if layer.geometryType() == QGis.Point:
            active_raster_layer.loadNamedStyle(str(qml_file))    # incompatible with Path()!
            # Set opacity:
            #active_raster_layer.setLayerTransparency(40)    # %    this method seems only be available for vector layer
            active_raster_layer.renderer().setOpacity(0.6)  # 0.6 = 40% transparency
            
            if self.dock.check_excl_zeroes.isChecked():
                self.out("setting zeroes to 100% transparency")
                '''
                # Set 0 values to NODATA (= transparent):
                provider = active_raster_layer.dataProvider()
                provider.setNoDataValue(1, 0)    # first one is referred to band number 
                # -> is working
                '''
                # better, conserves 0 value:
                tr = QgsRasterTransparency()
                tr.initializeTransparentPixelList(0)
                active_raster_layer.renderer().setRasterTransparency(tr)
            
            if hasattr(active_raster_layer, "setCacheImage"):
                active_raster_layer.setCacheImage(None)
            
            active_raster_layer.triggerRepaint()    # muss aufgerufen werden, Layer bleibt sonst schwarzweiß
            # Das ist für die QML-Farbskala im Layerfenster:
            #self.iface.legendInterface().refreshLayerSymbology(active_raster_layer)    # QGIS 2
            self.iface.layerTreeView().refreshLayerSymbology(active_raster_layer.id())
        # qml
        
        
        if write:
            yyyymmddHHMM = time.strftime("%Y%m%d_%H%M")
            new_file = model.data_root / yyyymmddHHMM + '.qgs'
            self.out("write: {}".format(new_file))
            project.write(new_file)
        
        
        
        #self.dock.button_box.button(QDialogButtonBox.Apply).setEnabled(False)
        #self.dock.btn_action.setEnabled(False)    # action was performed
        # -> better not, when user modify some options and wants to perform the same work again...
        
        # Close dock and perform work:
        self.dock.close()
        # !!! Close dock at the end, otherwise the isVisible() method on elements will return
        # False because the visibility of the parent element is also relevant !!!
        
        self.dock.tabWidget.setCurrentIndex(1)    # show statistics tab when open dock next, 0 ...
        
        
        self._load_print_layout(radolan_layer_name)
        self.out("*** Whole process finished! ***")
        
        self.iface.messageBar().pushMessage("Success", "Layer loaded", level=Qgis.Success, duration=3)
        
    
    
    def write_new_storage_location(self):
        ''' triggered by save button '''
        
        data_root_def_file = self._model.data_root_def_file
        path_to_save = self.dock.storage_dir
        
        # protection: check if writetable:
        if not os.access(path_to_save, os.W_OK):
            msg = 'directory "{}" is not writable!\nPlease select again.'.format(path_to_save)
            QMessageBox.critical(self.iface.mainWindow(), 'Write permission error', msg)
            return
        
        with open(data_root_def_file, 'w') as f:
            f.write(path_to_save)
        
        self.out("write_new_storage_location()")
        print("  saved path '{}' to file '{}'".format(path_to_save, data_root_def_file))
        
        self.dock.btn_save.setEnabled(False)    # show user, that action was performed
        
        # update new data path:
        self._model.data_root = Path(path_to_save)
        
        #self.dock.tabWidget.addTab(0, 'tab_action')    0 ...
        self.dock.tabWidget.setTabEnabled(0, True)
        self.dock.tabWidget.setTabEnabled(3, True)    # REGNIE
        # -> tab evtl. restored
        
    
    
    def ask_user_load_template_project(self):
        reply = QMessageBox.question(self.iface.mainWindow(), 'Continue?',
                 'Do you want to load template project?', QMessageBox.Yes, QMessageBox.No)
        
        if reply == QMessageBox.No:
            return
        
        
        """
        Related to Windows / QGIS 3.10
        Phenomenon: after loading the template project,
        QGIS map canvas 'jumps outside' the layer extents, even though the template project
        was saved with map centered to Germany.
        So one idea was to center on the german border shapefile after that (code below) but
        this hasn't worked.
        With the modification of the QSettings below the behaviour is OK now.
        """
        
        '''
        if platform.system() == 'Windows':
            # necessary?
            canvas = self.iface.mapCanvas()
            canvas.mapSettings().setDestinationCrs(QgsCoordinateReferenceSystem(3035))
            
            # Center on German border shape:
            vlayer_de_border = QgsVectorLayer(self._model.default_border_shape, 'de_border', 'ogr')
            #print("isValid:", vlayer_de_border.isValid())
            extent = vlayer_de_border.extent()
            canvas.setExtent(extent)
            canvas.refresh()
        '''
        
        """
        The following was related to CRS prompting, when loading a new layer,
        but it seems to fix the problem described above:
        "... Alternatively, you can also set up QGIS so that newly added layers use
        - the project's CRS
        - or a default CRS (in Settings/Options/CRS/Crs for new layers)
        
        You can also set those properties via the pyQGIS API
        QSettings().setValue('/Projections/defaultBehavior', 'useProject')  # Use project's crs
        or
        QSettings().setValue('/Projections/defaultBehavior', 'useGlobal')  # Use default crs
        QSettings().setValue('Projections/layerDefaultCrs', 'EPSG:4326')
        
        Note that the default value for '/Projections/defaultBehavior' is 'prompt' "
        
        https://gis.stackexchange.com/questions/288400/creating-a-memory-layer-without-the-crs-dialog-in-pyqgis-3/313872
        https://gis.stackexchange.com/questions/27745/how-can-i-specify-the-crs-of-a-raster-layer-in-pyqgis/27765#27765
        
        How does this work?
        An attempt to explain: by setting up in that way, that new layers follow the project crs,
        layers like german borders in WGS automatically follow the project crs (EPSG:3035) which
        is coded in the project template file on the one hand and additionally set up with crs externally.
        """
        settings = QSettings()
        #saved_setting = settings.value("/Projections/defaultBehavior")
        settings.setValue("/Projections/defaultBehavior", 'useProject')    # use project's crs
        
        
        self._model.create_default_project()
        
        # Show hint:
        msg = "Please save project immediately with a new name,\nnot to overwrite the template!"
        QMessageBox.warning(self.iface.mainWindow(), 'Save as new project', msg)
        
        # trigger the Project Save As menu button.
        self.iface.mainWindow().findChild( QAction, 'mActionSaveProjectAs' ).trigger()
        
    
    
    
    def _add_projection(self, projection_description, epsg_code):
        ''' add projection to ComboBox and a list with epsg codes,
        which corresponds each other. '''
        self.dock.cbbox_projections.addItem(projection_description)
        self._l_projections.append(epsg_code)
    
    
    def load_regnie(self):
        self.out("load_regnie()")
        
        regnie_raster_file = self.dock.text_regnie.text()
        
        # field was leaved empty
        if not regnie_raster_file:
            QMessageBox.critical(self.iface.mainWindow(), 'Layer loading error',
                                 "REGNIE raster input file wasn't specified!")
            return
        
        
        regnie_raster_file = Path(regnie_raster_file)
        
        if not regnie_raster_file.exists():
            QMessageBox.critical(self.iface.mainWindow(), 'Layer loading error',
                                 "File\n'{}'\nnot found!".format(regnie_raster_file))
            return
        
        
        self._model.set_data_dir('regnie')
        
        try:  # include everything what can raise exceptions
            try:
                self._model.create_storage_folder_structure()
                self.out("create data dir for converted REGNIE: '{}'".format(self._model.data_dir))
            except FileExistsError:
                pass
            csv_layer, regnie_max = self._model.convert_regnie_raster2csv_create_regnie_vector_layer(
                regnie_raster_file, self._model.data_dir)
        except Exception as e:
            QMessageBox.critical(self.iface.mainWindow(), 'Layer loading error', str(e))
            return
        
        
        QgsProject.instance().addMapLayer(csv_layer)
        
        # maybe to do... leastways reset RADOLAN fields
        self.dock.text_filename.setText(regnie_raster_file.name)
        self.dock.text_shape.setText("rows=971, cols=611")
        self.dock.text_max.setText(regnie_max)
        self.dock.text_min.setText(None)
        self.dock.text_mean.setText(None)
        self.dock.text_total_pixels.setText(None)
        self.dock.text_valid_pixels.setText(None)
        self.dock.text_nonvalid_pixels.setText(None)
        
        statistics_tab_index = 1    # 0 ...
        self.dock.tabWidget.setTabEnabled(statistics_tab_index, True)    # second tab "statistics"
        #self.dock.tabWidget.setCurrentIndex(statistics_tab_index)    # show statistics tab when open dock next
        # -> to little information yet
        
        
        
    
    def _load_print_layout(self, radolan_layer_name):
        
        window_title = "Print"    # will be checked with a found composer title
        
        """
        Avoid creating a new PrintComposer again and again here.
        Otherwise more and more composers will be added to the list - even with the same name.
        """
        
        project = QgsProject.instance()
        # From it, you can get the current layoutManager instance and deduce the layouts
        layout_manager = project.layoutManager()
        
        layout = layout_manager.layoutByName(window_title)
        
        if not layout:
            self.out("no composer found; creating one...")
            
            # Load the template into the composer
            # QGIS 2:
            #active_composer = self.iface.createNewComposer(window_title)    #createNewComposer()
            #active_composer = QgsComposition(QgsProject.instance())
            
            #layout = QgsLayout(project)
            layout = QgsPrintLayout(project)
            layout.initializeDefaults()    # initializes default settings for blank print layout canvas
            
            
            q_xmldoc = self._model.create_qdocument_from_print_template_content()
            
            # load layout from template and add to Layout Manager
            #layout.composition().loadFromTemplate(q_xmldoc)    # QGIS 2
            layout.loadFromTemplate(q_xmldoc, QgsReadWriteContext())
            layout.setName(window_title)
            
            layout_manager.addLayout(layout)
            
            
            # Update Logo:
            
            #logo_item = layout.getComposerItemById('logo')    # QGIS 2
            logo_item = layout.itemById('logo')
            #self.out(">>> logo_item: '{}'".format(logo_item))    # gibt nur die Objekt-ID aus
            logo_image = self._model.logo_path
            self.out("Logo: {}".format(logo_image))
            if logo_image.exists():
                logo_item.setPicturePath(str(logo_image))
            else:
                self.out("  ERROR: logo '{}' not found!".format(logo_image), False)
        # if
        
        
        """
        Hier versuche ich ein für die Überschrift mit einer ID ('headline') versehenes
        QgsLabel aus dem Template ausfindig zu machen. Ich mache das hier sehr kompliziert,
        es gibt bestimmt einen einfacheren Weg.
        Folgendes hat NICHT funktioniert:
        map_item = active_composer.getComposerItemById('headline')
        print(active_composer.items())
        liefert: [<PyQt4.QtGui.QGraphicsRectItem object at 0x124c60e8>, <qgis._core.QgsComposerLabel object at 0x124c65a8>, ... ]
        """
        
        ''' other possibility:
        for item in list(layout.items()):
            #if type(item) != QgsComposerLabel:
        '''
        
        # QgsComposerLabel:
        composer_label = layout.itemById('headline')    # a QgsComposerLabel was provided with the ID 'headline' in the template
        # -> None if not found
        
        if composer_label:
            prod_id  = self._ascii_converter.prod_id
            title    = self._model.title(self._ascii_converter.date, prod_id)
            subtitle = ""
            if prod_id != 'RW':
                subtitle = "{}-Produkt (Basis: RW)".format(prod_id)
            
            if prod_id != 'SF':
                if subtitle:
                    subtitle += '\n'
                subtitle += "für <Ort>"
            
            composer_label.setText( title + "\n" + subtitle )
        else:
            # A note that the template needs to be revised:
            self.out("no element with id 'headline' found!", False)
        
        
        
        legend = layout.itemById('legend')
        
        if not legend:
            self.out("legend couldn't created!", False)
            return
        
        
        #
        # Layer für die Legende ausfindig machen
        #
        
        # You would just need to make sure your layer has a name you can distinguish from others. Instead of:
        # Vorherige Version:
        #active_raster_layer = self.iface.activeLayer()
        
        # do:
        l_layer = project.mapLayersByName(radolan_layer_name)
        
        if not l_layer:
            self.out("legend: no layer found with name '{}'! (active_raster_layer=None)"
                     .format(radolan_layer_name), False)
            return
        
        active_raster_layer = l_layer[0]
        
        #print("Legend active_raster_layer id:",   active_raster_layer.id())     # ok
        #print("Legend active_raster_layer name:", active_raster_layer.name())   # ok
        #legend.model().setLayerSet([layer.id() for layer in layers])
        #legend.model().setLayerSet([active_raster_layer.id()])    # bringt nichts
        # DAS ist es! Dies fügt zumindest erstmal das interessierende Rasterlayer hinzu:
        #legend.modelV2().rootGroup().addLayer(active_raster_layer)
        #legend.updateLegend()
        
        #for layout in layout_manager.printLayouts():    # iterate layouts
        
        ''' would be ok, if we want to create a new legend -> then legend appears at the upper left corner
        legend = QgsLayoutItemLegend(layout)
        #legend.setTitle('Legend')
        legend.setAutoUpdateModel(False)
        group = legend.model().rootGroup()
        group.clear()
        group.addLayer(active_raster_layer)
        layout.addItem(legend)
        legend.adjustBoxSize()
        #legend.refresh()    # avoids adding all other layers 
        '''
        
        # uses existing legend object (see above), so we preserve it's layout position:
        legend.setAutoUpdateModel(False)
        group = legend.model().rootGroup()
        group.clear()
        group.addLayer(active_raster_layer)
        legend.adjustBoxSize()
        
        """ By default the newly created composer items have zero position (top left corner of the page) and zero size.
        The position and size are always measured in millimeters.
        # set label 1cm from the top and 2cm from the left of the page
        composerLabel.setItemPosition(20, 10)
        # set both label’s position and size (width 10cm, height 3cm)
        composerLabel.setItemPosition(20, 10, 100, 30)
        A frame is drawn around each item by default. How to remove the frame:
        composerLabel.setFrame(False)
        """
        #print(active_composer.rect().width(), active_composer.rect().height())                   # 1054 911
        #print(self.iface.mapCanvas().size().width(), self.iface.mapCanvas().size().height())     # 1517 535
        # "Leinwandgröße": habe keine vernünftigen Werte oben ermittelt (vielleicht Pixel; ich brauche mm).
        # selbst, mittels Mauszeiger ermittelt:
        width  = 210
        height = 297
        # Rand neben der Legende (mm):
        dw = 10
        dh = 14
        
        """
        Doesn't work since QGIS 3:
        """
        #self.out("In QGIS 3 the print layout doesn't work anymore. If you can do it ...", False)
        
        # nothing works
        #legendSize = legend.paintAndDetermineSize(None)
        #legend.setItemPosition(width - legendSize.width() - dw,  height - legendSize.height() - dh)
        #legend.setItemPosition(width - legend.width() - dw,  height - legend.height() - dh)
        
        
        # Also note that active_composer.composerWindow() has a hide() and show()
        #active_composer.composerWindow().hide()    # works
        
    
    